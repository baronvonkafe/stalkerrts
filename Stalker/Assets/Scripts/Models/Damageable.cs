using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Damageable : MonoBehaviour
{
    [SerializeField] [ReadOnly] Progressbar hpBar;

    void Awake()
    {
        hpBar = transform.GetChild(0).GetChild(0).GetComponent<Progressbar>();
    }

    public void OnDamageTaken(float damage)
    {
        hpBar.OnValueChanged();
    } 

}
