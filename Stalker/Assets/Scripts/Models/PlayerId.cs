﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerId
{
    public string playerName;
    public int playerNumber;
    public Color playerColor;
}
