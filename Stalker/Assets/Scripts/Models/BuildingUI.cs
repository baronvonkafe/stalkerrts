﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BuildingUI : MonoBehaviour
{
    
    private bool isPlacing = false;
    private int currentIndex = 0;
    private GameObject buildingGO;
    private BuildingUnit building;
    private Vector3Int previous;
    private Color placeableColor = Color.green;
    private Color disableColor = Color.red;
    private Color transparentColor = new Color(1, 1, 1, 0.5f);

    public Player player; //Must be fixed for later multiplayer use.
    public GameObject buildingButton;
    public List<Button> buildingButtons = new List<Button>();

    void Start()
    {
        int totalButtons = BuildingDistributor.Instance.GetAllPrefabCount();

        for (int i = 0; i < totalButtons; i++)
        {
            GameObject buttonGO = Instantiate(buildingButton, this.transform);
            Button button = buttonGO.GetComponent<Button>();
            int buttonIndex = i;
            button.onClick.AddListener(() => SelectBuilding(buttonIndex));

            BuildingUnit b = BuildingDistributor.Instance.GetBuilding(i);
            button.GetComponentInChildren<TextMeshProUGUI>().text = b.name;
            
            if(b.GetIsLocked())
            {
                buttonGO.SetActive(false);
            }

            buildingButtons.Add(button);

            //Replace later with an image?
        }
    }

    void Update()
    {
        //if (!hasAuthority)
        //    return;

        if (isPlacing && building != null)
        {
            Vector3Int currentCellPos = GridManager.Instance.GetCurrentCursorCellPos();
            bool canBePlaced = CanBePlaced(currentCellPos);
            HighlightGround(currentCellPos, canBePlaced);
            Debug.Log(currentCellPos);

            if (currentCellPos != previous)
            {
                GridManager.Instance.GetHighlightMap().SetTile(currentCellPos, building.GetTileBuilding());
                GridManager.Instance.GetHighlightMap().SetTile(previous, null);

                GridManager.Instance.GetGroundMap().SetColor(previous, Color.white);

                previous = currentCellPos;
            }

            if (Input.GetMouseButtonDown(0) && canBePlaced)
            {
                StartContruction(currentCellPos);
            }
            else if(Input.GetMouseButtonDown(1) || Input.GetKey(KeyCode.Escape))
            {
                isPlacing = false;
                GridManager.Instance.GetGroundMap().SetColor(previous, Color.white);
                GridManager.Instance.GetHighlightMap().SetTile(currentCellPos, null);
            }
        }
    }

    void SelectBuilding(int index)
    {
        buildingGO = Instantiate(BuildingDistributor.Instance.GetPrefab(index));
        building = buildingGO.GetComponent<BuildingUnit>();

        if(player.CanPlayerAffordResources(building.GetBuildingCost()))
        {
            building.ChangeBuildingColor(transparentColor);
            currentIndex = index;
            isPlacing = true;
            Destroy(buildingGO);
        }
        else
        {
            Destroy(buildingGO);
            building = null;
        }
    }

    private bool CanBePlaced(Vector3Int position)
    {
        bool retValue = false;

        if (!GridManager.Instance.GetBuildingMap() || !building)
            return retValue;

        if(!GridManager.Instance.GetBuildingMap().HasTile(position))
        {
            retValue = true;
        }

        return retValue;
    }

    private void StartContruction(Vector3Int currentCellPos)
    {
        isPlacing = false;
        player.SubstractResources(building.GetBuildingCost());
        building.ChangeBuildingColor(Color.white);
        building.SetTilePosition(currentCellPos);
        //GridManager.Instance.SetBuilding(buildingGO, true, player.gameObject);
        player.AddBuilding(buildingGO, building.GetTilePosition(), true);
        GridManager.Instance.GetHighlightMap().SetTile(currentCellPos, null);
        GridManager.Instance.GetGroundMap().SetColor(previous, Color.white);
    }

    private void HighlightGround(Vector3Int position, bool canBePlaced)
    {
        if (!GridManager.Instance.GetBuildingMap().HasTile(position))
        {
            GridManager.Instance.GetGroundMap().SetColor(position, placeableColor);
        }
        else
        {
            GridManager.Instance.GetGroundMap().SetColor(position, disableColor);
        }
    }
}
