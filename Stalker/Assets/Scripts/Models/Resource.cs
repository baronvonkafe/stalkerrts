﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct Resource
{
    [SerializeField] public ResourceType resourceType;
    [SerializeField] public int amount;

}
