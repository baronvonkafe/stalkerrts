﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

[RequireComponent(typeof(Image))]
public class TabButtonUI : MonoBehaviour, IPointerClickHandler,
     IPointerDownHandler,
     IPointerUpHandler,
     IPointerEnterHandler,
     IPointerExitHandler,
     ISelectHandler
{

    private TabGroup tabGroup;

    public Image background;

    public UnityEvent onTabSelected;
    public UnityEvent onTabDeselected;

    private void Start()
    {
        background = GetComponent<Image>();
        tabGroup = GetComponentInParent<TabGroup>();
        tabGroup.Subscribe(this);

        if (!tabGroup)
        {
            Debug.LogError("TabGroup not found in: " + this.transform.parent.gameObject.name);
            gameObject.SetActive(false);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        tabGroup.OnTabSelected(this);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        tabGroup.OnTabEnter(this);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        tabGroup.OnTabExit(this);
    }

    public void OnPointerDown (PointerEventData eventData)
     {
         
     }
     public void OnPointerUp (PointerEventData eventData)
     {
         
     }

     public void OnSelect (BaseEventData eventData)
     {
         
     }

    public void Select()
    {
        if(onTabSelected != null)
        {
            onTabSelected.Invoke();
        }
    }
    public void DeSelect()
    {
        if(onTabDeselected != null)
        {
            onTabDeselected.Invoke();
        }
    }
}
