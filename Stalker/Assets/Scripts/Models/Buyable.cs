using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Timer))]
public class Buyable : MonoBehaviour
{
    [SerializeField] protected List<Resource> resourceCost = new List<Resource>();
    [SerializeField] float totalWorkToComplete = 5;
    [ReadOnly] [SerializeField] Timer contructionTimer;

    void Awake()
    {
        contructionTimer = GetComponent<Timer>();
        contructionTimer.SetGoalTimer(totalWorkToComplete);
        contructionTimer.onFinish.AddListener(ConstructionUnitFinished);
    }

    public void ContructUnitStart()
    {
        contructionTimer.Start();
        // Play contruction sounds
        // Update progressbar
    }

    public void ConstructionUnitFinished()
    {
        Debug.Log(this.name + " has finished construction");
    }

    public List<Resource> GetBuildingCost()
    {
        return resourceCost;
    }
}