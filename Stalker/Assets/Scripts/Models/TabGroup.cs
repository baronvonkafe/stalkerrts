﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TabGroup : MonoBehaviour
{
    List<TabButtonUI> tabButtons;
    public Sprite tabIdle;
    public Sprite tabHover;
    public Sprite tabActive;
    public TabButtonUI selectedTab;
    public List<GameObject> objectsToSwap;

    public void Subscribe(TabButtonUI button)
    {
        if(tabButtons == null)
        {
            tabButtons = new List<TabButtonUI>();
        }

        tabButtons.Add(button);
    }

    public void OnTabEnter(TabButtonUI button)
    {
        ResetTabs();
        if(selectedTab == null || button != selectedTab)
        {
            button.background.sprite = tabHover;
        }
    }

    public void OnTabExit(TabButtonUI button)
    {
        ResetTabs();
    }

    public void OnTabSelected(TabButtonUI button)
    {
        if(selectedTab != null)
        {
            selectedTab.DeSelect();
        }

        selectedTab = button;

        selectedTab.Select();

        ResetTabs();
        button.background.sprite = tabActive;
        int index = button.transform.GetSiblingIndex();
        
        for(int i = 0; i < objectsToSwap.Count; i++)
        {
            if(i == index)
            {
                objectsToSwap[i].SetActive(true);
            }
            else
            {
                objectsToSwap[i].SetActive(false);
            }
        }
    }

    public void ResetTabs()
    {
        foreach (TabButtonUI button in tabButtons)
        {
            if(selectedTab != null && button == selectedTab){ continue; }
            button.background.sprite = tabIdle;
        }
    }
}
