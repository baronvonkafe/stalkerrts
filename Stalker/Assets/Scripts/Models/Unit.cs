using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Damageable))]
[RequireComponent(typeof(Buyable))]
public class Unit : MonoBehaviour
{

    protected Damageable damageable;
    protected Buyable buyable;

    [ReadOnly][SerializeField] protected Vector3Int tilePosition;
    [SerializeField] protected Vector2Int tileSize = new Vector2Int(1, 1);
    [SerializeField] protected bool locked;
    [ReadOnly] [SerializeField] Progressbar loadingBar;
    
    protected GameObject owner;
    
    void Awake()
    {
        damageable = GetComponent<Damageable>();
        buyable = GetComponent<Buyable>();
        loadingBar = transform.GetChild(0).GetChild(1).GetComponent<Progressbar>();
    }


    public void SetTilePosition(Vector3Int pos)
    {
        tilePosition = pos;
    }

    public Vector3Int GetTilePosition()
    {
        return tilePosition;
    }

    public void SetLocked(bool isLocked)
    {
        locked = isLocked;
    }

    public bool GetIsLocked()
    {
        return locked;
    }
}
