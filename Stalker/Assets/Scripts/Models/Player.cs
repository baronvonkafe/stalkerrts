﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public PlayerId playerId;
    private UI ui;
    [ReadOnly] private Resource[] currentResources = new Resource[Consts.MAX_RESOURCES];
    public GameObject buildings;
    

    public void Start()
    {
        ui = GetComponentInChildren<UI>();
        buildings = transform.GetChild(2).gameObject;

        for (int i = 0; i < Consts.MAX_RESOURCES; i++)
        {
            currentResources[i].resourceType = (ResourceType)i;
        }
        Vector3Int startingPosition = BuildingDistributor.Instance.GiveRandomPlayerStartingPosition();
        AddBuilding(BuildingDistributor.Instance.GetPrefab(0), startingPosition, false);
        //GridManager.Instance.SetBuilding(0, startingPosition, false, this.gameObject);
        GetComponent<CameraController>().SetCameraStartPosition(GridManager.Instance.GetBuildingMap().CellToWorld(startingPosition));
    }

    public void AddResource(ResourceType resourceType, int amount)
    {
        if (amount == 0)
            return;

        for(int i = 0; i < Consts.MAX_RESOURCES; i++)
        {
            if(currentResources[i].resourceType == resourceType)
            {
                currentResources[i].amount += amount;
            }
        }

        if (ui)
            ui.RefreshResources();
    }

    public void AddResource(Resource resource)
    {
        AddResource(resource.resourceType, resource.amount);
    }

    public void SubstractResource(ResourceType resourceType, int amount)
    {
        AddResource(resourceType, -amount);
    }

    public void SubstractResource(Resource resource)
    {
        SubstractResource(resource.resourceType, resource.amount);
    }

    public void SubstractResources(List<Resource> resources)
    {
        for (int i = 0; i < resources.Count; i++)
        {
            SubstractResource(resources[i]);
        }
    }

    public bool CanPlayerAffordResource(Resource resource)
    {
        bool retValue = true;

        if(resource.amount > 0)
        {
            if(currentResources[(int)resource.resourceType].amount < resource.amount)
            {
                retValue = false;
            }
        }

        return retValue;
    }

    public bool CanPlayerAffordResources(List<Resource> resources)
    {
        bool retValue = true;

        for(int i = 0; i < resources.Count; i++)
        {
            if (!CanPlayerAffordResource(resources[i]))
            {
                retValue = false;
            }
        }

        return retValue;
    }

    public Resource[] GetCurrentResources()
    {
        return currentResources;
    }

    public void AddBuilding(GameObject building, Vector3Int tilePosition, bool construction)
    {
        Vector3 pos = GridManager.Instance.GetBuildingMap().CellToWorld(tilePosition);
        GameObject go = Instantiate(building, pos, Quaternion.identity, buildings.transform);
    }

    public void DestroyBuilding(GameObject building)
    {
        // Removes building from the player building handler
    }
}
