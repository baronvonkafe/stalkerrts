﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BuildingUnit : Unit
{

    [SerializeField] private Tile constructionTile;
    [SerializeField] private Tile buildingTile;

    [ReadOnly] public bool isHover = false;
    [ReadOnly] public bool isSelected = false;
    private bool done;


    public void StartBuilding()
    {
        
    }

    public void OnDamageTaken(float damage)
    {
        damageable.OnDamageTaken(damage);
    }

    public void OnMouseEnter()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Debug.Log(this.name + " clicked");
            isHover = true;
        }
    }

    public void OnMouseExit()
    {
        isHover = false;
    }

    public void SetOwner(GameObject newOwner)
    {
        owner = newOwner;
    }

    public GameObject GetOwner()
    {
        return owner;
    }

    public List<Resource> GetBuildingCost()
    {
        return buyable.GetBuildingCost();
    }

    public Tile GetTileBuilding()
    {
        return buildingTile;
    }

    public Tile GetTileConstruction()
    {
        return constructionTile;
    }

    public void ChangeBuildingColor(Color color)
    {
        buildingTile.color = color;
    }
}
