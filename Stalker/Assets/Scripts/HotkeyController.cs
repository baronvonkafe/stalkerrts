﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotkeyController : MonoBehaviour
{
    Player player;

    void Start()
    {   
        player = GetComponent<Player>();
    }

    void Update()
    {
        CheckInputs();
    }

    void CheckInputs()
    {
        if(Input.GetKeyDown(KeyCode.F1))
        {
            GiveResources();
        }
    }

    private void GiveResources()
    {
        player.AddResource(ResourceType.Population, 100);
        player.AddResource(ResourceType.Food, 100);
        player.AddResource(ResourceType.Wood, 100);
        player.AddResource(ResourceType.Stone, 100);
    }
}
