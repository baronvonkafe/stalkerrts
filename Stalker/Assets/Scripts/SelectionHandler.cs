using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionHandler : MonoBehaviour
{   
    public static Rect selection = new Rect(0, 0, 0, 0);
    private Vector3 startClickPosition = -Vector3.one;
    public Texture2D selectionHighlight = null;
    private List<MovableUnit> selectedUnits = new List<MovableUnit>();

    void Update()
    {
        ProcessSelection();
    }

    private void SelectPressed()
    {
        startClickPosition = Input.mousePosition;

        RaycastHit hit;

        Ray ray = Camera.main.ScreenPointToRay(GridManager.Instance.GetCurrentCursorCellPos());

        Vector2 point = new Vector2();

        /*if(GridManager.Instance.GetBuildingMap().HasTile(GridManager.Instance.GetCurrentCursorCellPos()))
        {
            Debug.Log(GridManager.Instance.GetBuildingMap().GetTile(GridManager.Instance.GetCurrentCursorCellPos()).name);
        }*/
    }
    


    private void SelectReleased()
    {
        Vector3 currentMousePos = Input.mousePosition;
        Collider2D[] collider2DArray = Physics2D.OverlapAreaAll(Camera.main.ScreenToWorldPoint(startClickPosition), Camera.main.ScreenToWorldPoint(currentMousePos));

        //Debug.Log("Cell: " + GridManager.Instance.GetCurrentCursorCellPos() + ", has building: " + GridManager.Instance.HasBuildingOnTheCell());

        selectedUnits.Clear();

        foreach(Collider2D collider2D in collider2DArray)
        {
            MovableUnit unitRTS = collider2D.GetComponent<MovableUnit>();
            if(unitRTS != null)
            {
                selectedUnits.Add(unitRTS);
            }
        }

        startClickPosition = -Vector3.one;
    }
    

    private void ProcessSelection()
    {
        if(startClickPosition != -Vector3.one)
        {
            
            Vector3 currentMousePos = Input.mousePosition;

            selection = new Rect(startClickPosition.x, Screen.height - startClickPosition.y, currentMousePos.x - startClickPosition.x, (Screen.height - currentMousePos.y) - (Screen.height - startClickPosition.y));
            if (selection.width < 0)
            {
                selection.x += selection.width;
                selection.width = -selection.width;
            }
            if (selection.height < 0)
            {
                selection.y += selection.height;
                selection.height = -selection.height;
            }
        }
    }

    private void OnGUI()
    {
        if(startClickPosition != -Vector3.one && selectionHighlight)
            GUI.DrawTexture(selection, selectionHighlight);
    }
}
