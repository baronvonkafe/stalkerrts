﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GridManager : Singleton<GridManager>
{
    private Tilemap waterMap;
    private Tilemap groundMap;
    private Tilemap baseMap;
    private Tilemap buildingsMap;
    private Tilemap highlightMap;
    private Tilemap minimapMap;

    private void Start()
    {
        waterMap = transform.GetChild(0).GetComponent<Tilemap>();
        groundMap = transform.GetChild(1).GetComponent<Tilemap>();
        baseMap = transform.GetChild(2).GetComponent<Tilemap>();
        buildingsMap = transform.GetChild(3).GetComponent<Tilemap>();
        highlightMap = transform.GetChild(4).GetComponent<Tilemap>();
        minimapMap = transform.GetChild(5).GetComponent<Tilemap>();
    }

    public Tilemap GetWaterMap()
    {
        return waterMap;
    }

    public Tilemap GetGroundMap()
    {
        return groundMap;
    }

    public Tilemap GetBaseMap()
    {
        return baseMap;
    }

    public Tilemap GetBuildingMap()
    {
        return buildingsMap;
    }

    public Tilemap GetHighlightMap()
    {
        return highlightMap;
    }

    public Vector3Int GetCurrentCursorCellPos()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3Int currentCellPos = GridManager.Instance.GetHighlightMap().WorldToCell(mousePos);
        return currentCellPos;
    }

    public Vector2 GetCurrentCursorWorldPos()
    {
        Vector2 retValue = new Vector2();

        return retValue;
    }

    public void SetBuilding(BuildingUnit building, bool construction)
    {
        if(construction)
            buildingsMap.SetTile(building.GetTilePosition(), building.GetTileConstruction());
        else
            buildingsMap.SetTile(building.GetTilePosition(), building.GetTileBuilding());
    }

    public void SetBuilding(GameObject building, bool construction, GameObject owner)
    {
        Vector3 pos = buildingsMap.CellToWorld(building.GetComponent<BuildingUnit>().GetTilePosition());
        building.GetComponent<BuildingUnit>().SetOwner(owner);
        GameObject go = Instantiate(building, pos, Quaternion.identity);
        go.transform.SetParent(buildingsMap.transform);
        Debug.Log(pos);
    }

    public bool HasBuildingOnTheCell()
    {
        bool hasBuilding = false;

        if(buildingsMap.HasTile(GetCurrentCursorCellPos()))
        {
            hasBuilding = true;
        }

        return hasBuilding;
    }

    public void SetBuilding(int prefabIndex, Vector3Int tilePosition, bool construction, GameObject owner)
    {
        SetBuilding(BuildingDistributor.Instance.GetPrefab(prefabIndex), tilePosition, construction, owner);
    }

    public void SetBuilding(GameObject building, Vector3Int tilePosition, bool construction, GameObject owner)
    {
        Vector3 pos;
        GameObject go;
        BuildingUnit buildingUnit;

        pos = buildingsMap.CellToWorld(tilePosition);
        go = Instantiate(building, pos, Quaternion.identity, buildingsMap.transform);
        buildingUnit = go.GetComponent<BuildingUnit>();
        buildingUnit.SetOwner(owner);
        buildingUnit.SetTilePosition(tilePosition);

        if(construction)
            buildingsMap.SetTile(tilePosition, buildingUnit.GetTileConstruction());
        else
            buildingsMap.SetTile(tilePosition, buildingUnit.GetTileBuilding());

        
        
    }
}
