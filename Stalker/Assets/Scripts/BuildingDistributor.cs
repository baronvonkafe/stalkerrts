using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingDistributor : Singleton<BuildingDistributor>
{
    public List<Vector3Int> playerStartingPos = new List<Vector3Int>();
    [SerializeField] private GameObject[] buildingPrefabs;

    public Vector3Int GiveRandomPlayerStartingPosition()
    {
        Vector3Int retValue = Vector3Int.zero;
        int index = 0;

        if(playerStartingPos.Count == 0)
        {
            Debug.LogError("No player starting positions left!");
            return retValue;
        }

        index = Random.Range(0, playerStartingPos.Count);
        retValue = playerStartingPos[index];
        playerStartingPos.RemoveAt(index);

        return  retValue;
    }

    public int GetAllPrefabCount()
    {
        return buildingPrefabs.Length;
    }

    public GameObject GetPrefab(int index)
    {
        if (index < buildingPrefabs.Length && index >= 0) return buildingPrefabs[index];
        Debug.LogError("GetPrefab index" + index + " exceeds the prefab list!");
        return null;
    }

    public BuildingUnit GetBuilding(int index)
    {
        return buildingPrefabs[index].GetComponent<BuildingUnit>();
    }

}
