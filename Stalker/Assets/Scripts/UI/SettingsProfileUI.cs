﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SettingsProfileUI : MonoBehaviour
{
    public TMP_InputField playerName;
    public Color playerColor;

    public void Start()
    {
        playerName.text = PlayerPrefs.GetString("PlayerName");
    }

    public void SetPlayerName(string name)
    {
        PlayerPrefs.SetString("PlayerName", playerName.text);
    }

}
