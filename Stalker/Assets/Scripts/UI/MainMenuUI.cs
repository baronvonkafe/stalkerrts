﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using MLAPI;

public class MainMenuUI : MonoBehaviour
{
    public GameObject settings;
    public GameObject multiplayer;
    public GameObject mapEditor;
    

    void Start()
    {
        DisableAllPanels();
    }

    public void StartGame()
    {
        NetworkManager.Singleton.StartHost();
        //SceneManager.LoadScene("Game", LoadSceneMode.Single);
    }

    public void OpenSettings()
    {
        if (!settings.activeInHierarchy)
        {
            DisableAllPanels();
            settings.SetActive(true);
        }
    }

    public void OpenMultiplayer()
    {
        if(!multiplayer.activeInHierarchy)
        {
            DisableAllPanels();
            multiplayer.SetActive(true);
        }
    }

    public void OpenMapEditorMenu()
    {
        if(!mapEditor.activeInHierarchy)
        {
            DisableAllPanels();
            mapEditor.SetActive(true);
        }
    }

    private void DisableAllPanels()
    {
        settings.SetActive(false);
        multiplayer.SetActive(false);
        mapEditor.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
