using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Progressbar : MonoBehaviour
{
    const int TICK_RANGE = 25; 
    public int minimum;
    public int maximum;
    public int current;
    public Image mask;
    public Image fill;
    private GameObject tick;
    private List<GameObject> ticks = new List<GameObject>();
    public Color color;
    public bool useTicks = true;
    public float tickWidth = 0.01f;

    public UnityEvent valueChanged;

    #if UNITY_EDITOR
    [MenuItem("GameObject/UI/Linear Progressbar")]
    public static void AddLinearProgressBar()
    {
        GameObject obj = Instantiate(Resources.Load<GameObject>("UI/Linear Progressbar"));
        obj.transform.SetParent(Selection.activeGameObject.transform, false);
    }
#endif

    void Start()
    {
        GetCurrentFill();
        
        tick = Instantiate(Resources.Load<GameObject>("UI/ProgressBarTick"), this.transform, false);
        tick.SetActive(false);

        if(useTicks)
        {
            UpdateTicks();
        }
    }

    public void OnValueChanged()
    {
        GetCurrentFill();

        if(useTicks)
        {
            UpdateTicks();
        }
    }

    void GetCurrentFill()
    {
        float currentOffset = current - minimum;
        float maximumOffset = maximum - minimum;
        float fillAmount = (float)currentOffset / (float)maximumOffset;
        mask.fillAmount = fillAmount;
        fill.color = color;
    }

    void UpdateTicks()
    {
        for (int i = 0; i < fill.transform.childCount; i++)
        {
            Destroy(fill.transform.GetChild(i).gameObject);
        }

        ticks.Clear();

        
        int numberOfTicksNeeded = Mathf.FloorToInt(maximum / TICK_RANGE);
        
        for (int i = 0; i < numberOfTicksNeeded; i++)
        {
            if(i > 0)
            {
                ticks.Add(Instantiate(tick));
                ticks[i-1].transform.SetParent(fill.transform, false);
                float tickPos = (fill.rectTransform.rect.width / numberOfTicksNeeded) * i;
                ticks[i-1].GetComponent<RectTransform>().sizeDelta = new Vector2(tickWidth, ticks[i-1].GetComponent<RectTransform>().sizeDelta.y);
                ticks[i-1].GetComponent<RectTransform>().anchoredPosition = new Vector2(tickPos, 0);
                ticks[i-1].SetActive(true);
            }
        }
    }
}
