﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsGameplayUI : MonoBehaviour
{

    public Toggle cursorLock;
    bool lockedCursor;

    private void Start()
    {
        cursorLock.isOn = Convert.ToBoolean(PlayerPrefs.GetInt("CursorLock"));
    }

    public void LockCursor(bool lockCursor)
    {
        lockedCursor = lockCursor;
        PlayerPrefs.SetInt("CursorLock", Convert.ToInt32(lockedCursor));
    }

}
