﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UI : MonoBehaviour
{
    public RectTransform resourceParent;
    Player player;

    void Start()
    {
        player = GetComponentInParent<Player>();
    }

    public void RefreshResources()
    {
        for (int i = 0; i < resourceParent.childCount; i++)
        {
            resourceParent.GetChild(i).GetComponentInChildren<TextMeshProUGUI>().text = player.GetCurrentResources()[i].amount.ToString();
        }
    }


}
