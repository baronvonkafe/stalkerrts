﻿using System.Security.AccessControl;
using System;
using System.Reflection;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class GameMenuUI : MonoBehaviour
{
    public GameObject settings;

    private void Awake()
    {
        this.gameObject.SetActive(false);
        settings.SetActive(false);
    }

    public void SavePressed()
    {
        
    }

    public void LoadPressed()
    {

    }

    public void SettingsPressed()
    {
        settings.SetActive(true);
    }

    public void QuitPressed()
    {
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }
}
