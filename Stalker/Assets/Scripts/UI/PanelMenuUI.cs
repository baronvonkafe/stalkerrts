﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelMenuUI : MonoBehaviour
{
    public void ClosePanel()
    {
        this.gameObject.SetActive(false);
    }
}
