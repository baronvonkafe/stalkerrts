using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitDistributor : Singleton<UnitDistributor>
{

    [SerializeField] private List<GameObject> unitPrefabs;

    public GameObject GetPrefab(int index)
    {
        if (index < unitPrefabs.Count && index >= 0) return unitPrefabs[index];
        Debug.LogError("GetPrefab index" + index + " exceeds the prefab list!");
        return null;
    }

    public int GetAllPrefabCount()
    {
        return unitPrefabs.Count;
    }

    public void AddCustomUnit(GameObject customUnit)
    {
        unitPrefabs.Add(customUnit);
    }

}
