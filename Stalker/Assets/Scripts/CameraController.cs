﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CameraController : MonoBehaviour 
{
    Camera cam;
    public float cameraSpeed;
    private float cameraZoom;
    public float cameraZoomSpeed = 0.5f;
    
    private float mouseScrollY;
    private Vector2 movementDirection;
    private Vector2 mousePos;

    private float defaultZoom = 2.5f;
    public Vector2 zoomLimits = new Vector2(1, 5);

    private Vector2 dragPos;
    private Vector2 dragPosStart;
    private Vector2 camDirection;

    private bool isDragging = false;
    private float screenMoveTreshold = 0.005f;
    public bool gamePaused = false;
    public GameMenuUI gameMenu;

    void Awake()
    {
        cam = GetComponentInChildren<Camera>();
        cam.gameObject.SetActive(true);
        cam.orthographicSize = defaultZoom;
        cameraZoom = cam.orthographicSize;
    }

    private void Update()
    {
        //if (!hasAuthority)
        //    return;

        movementDirection = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        movementDirection = movementDirection.normalized;
        mouseScrollY = Input.GetAxisRaw("Mouse ScrollWheel");
        
        HandleDragInputs();
        
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            EscapePressed();
        }

        mousePos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        camDirection = Vector2.zero;

        UpdateCameraZoom();
        UpdateCameraDirection();
    }

    private void HandleDragInputs()
    {
        if(Input.GetMouseButtonDown(2))
        {
            DragPressed();
        }

        if(Input.GetMouseButton(2))
        {
            isDragging = true;
        }
        else
        {
            isDragging = false;
        }

        if(Input.GetMouseButtonUp(2))
        {
            dragPosStart = Vector3.zero;
        }
    }

    public void SetCameraStartPosition(Vector3 startPos)
    {
        startPos.z = -10;
        //startTilePos = new Vector3Int(startTilePos.x, startTilePos.y, -10);
        cam.transform.position = startPos;
        //Debug.Log(GridManager.Instance.GetBuildingMap().CellToWorld(startPos));
    }

    public void EscapePressed()
    {
        if (gameMenu.gameObject.activeInHierarchy)
        {
            ResumeGame();
            gameMenu.gameObject.SetActive(false);
        }
        else
        {
            PauseGame();
            gameMenu.gameObject.SetActive(true);
        }
    }

    private void UpdateCameraZoom()
    {
        if (mouseScrollY != 0)
        {
            cam.orthographicSize = Mathf.Clamp(cam.orthographicSize - (mouseScrollY * cameraZoomSpeed * Time.deltaTime), zoomLimits.x, zoomLimits.y);
        }
    }

    private void UpdateCameraDirection()
    {
        if (movementDirection != Vector2.zero)
        {
            camDirection = movementDirection;
        }

        if (isDragging)
        {
            camDirection = (mousePos - dragPosStart).normalized;
        }
        else
        {
            Vector2 borderMovement = GetCursorCloseToBorderMovement(mousePos);

            if (borderMovement != Vector2.zero)
                camDirection = borderMovement;
        }

        if (camDirection != Vector2.zero)
        {
            if(!gamePaused)
                cam.transform.Translate(GetCameraMovement(camDirection));
        }
    }

    private void DragPressed()
    {
        dragPosStart = cam.ScreenToViewportPoint(Input.mousePosition);
    }

    private bool IsOnCameraArea(Vector2 pos)
    {
        if (pos.x >= -0.001f && pos.x <= 1 && pos.y >= -0.001f && pos.y <= 1)
        {
            return true;
        }
        return false;
    }

    private Vector3 GetCameraMovement(Vector2 direction)
    {
        Vector3 mov = direction * (cameraSpeed * (cam.orthographicSize / 2) * Time.deltaTime);

        return mov;
    }

    private Vector2 GetCursorCloseToBorderMovement(Vector2 pos)
    {
        Vector2 mov = new Vector2();

        if (pos.x <= screenMoveTreshold && pos.x >= 0)
            mov.x = -1f;
        else if (pos.x > 1.0f - screenMoveTreshold && pos.x < 1.0f)
            mov.x = 1f;

        if (pos.y <= screenMoveTreshold && pos.y >= 0)
            mov.y = -1f;
        else if (pos.y > 1.0f - screenMoveTreshold && pos.y < 1.0f)
            mov.y = 1f;

        return mov;
    }

    public void PauseGame()
    {
        gamePaused = true;
    }

    public void ResumeGame()
    {
        gamePaused = false;
    }
}
