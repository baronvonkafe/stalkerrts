﻿using System;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;

public class GameController : Singleton<GameController>
{
    private void Start()
    {
        if(Convert.ToBoolean(PlayerPrefs.GetInt("CursorLock")))
        {
            Cursor.lockState = CursorLockMode.Confined;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }


    void Update()
    {
        if(Input.GetKeyUp(KeyCode.F12))
        {
            QuickStartGame();
        }
    }
    
    void QuickStartGame()
    {
        NetworkManager.Singleton.StartHost();
    }
}
