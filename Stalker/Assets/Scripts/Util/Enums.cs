﻿public enum GameStates
{
    MainMenu,
    Game,
    EndGameScreen
}

public enum ResourceType
{
    Population,
    Food,
    Stone,
    Wood,
    Gold,
    Ammo,
    Oil,
    Metal,
    Temp
}

public enum BuildingType
{
    Resource,
    Defence
}
