using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{
    bool active = false;
    
    public float goalTime = 5f;
    float currentTime = 0f;
    
    public UnityEvent onFinish;

    void Update()
    {
        if(active)
        {
            currentTime -= Time.deltaTime;

            if(currentTime >= goalTime)
            {
                if(onFinish != null)
                {
                    currentTime = goalTime;
                    onFinish.Invoke();
                }
            }
        }
    }

    public void Start()
    {
        active = true;
        Reset();
    }

    public void Pause()
    {
        active = false;
    }

    public void Resume()
    {
        active = true;
    }

    public void Reset()
    {
        currentTime = goalTime;
    }

    public bool IsActive()
    {
        return active;
    }

    public int GetProgressPercent()
    {
        return Mathf.RoundToInt(1 - (currentTime / goalTime)) * 100;
    }

    public float GetProgress()
    {
        return 1 - (currentTime / goalTime);
    }

    public float GetRemainingTime()
    {
        return currentTime;
    }

    public int GetRemainingTimeInt()
    {
        return Mathf.RoundToInt(currentTime);
    }

    public void SetGoalTimer(float goal)
    {
        this.goalTime = goal;
    }
}
